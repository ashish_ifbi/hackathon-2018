 Welcome back, nick@nickcelestin.com
Logout
My Locations
My Hubs
My Devices
My SmartApps
My Device Handlers
My Publication Requests
Live Logging
Documentation
It's Too Hot

/**
 *  Copyright 2015 SmartThings
 *
 *  Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 *  in compliance with the License. You may obtain a copy of the License at:
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software distributed under the License is distributed
 *  on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License
 *  for the specific language governing permissions and limitations under the License.
 *
 *  It's Too Hot
 *
 *  Author: SmartThings
 */
definition(
    name: "It's Too Hot",
    namespace: "smartthings",
    author: "SmartThings",
    description: "Monitor the temperature and when it rises above your setting get a notification and/or turn on an A/C unit or fan.",
    category: "Convenience",
    iconUrl: "https://s3.amazonaws.com/smartapp-icons/Meta/its-too-hot.png",
    iconX2Url: "https://s3.amazonaws.com/smartapp-icons/Meta/its-too-hot@2x.png"
)
​
preferences {
    section("Monitor the temperature...") {
        input "temperatureSensor1", "capability.temperatureMeasurement"
    }
    section("When the temperature rises above...") {
        input "temperature1", "number", title: "Temperature?"
    }
    section( "Notifications" ) {
        input("recipients", "contact", title: "Send notifications to") {
            input "sendPushMessage", "enum", title: "Send a push notification?", options: ["Yes", "No"], required: false
            input "phone1", "phone", title: "Send a Text Message?", required: false
        }
    }
    section("Turn on which A/C or fan...") {
        input "switch1", "capability.powerMeter", required: false
    }
}
Logs
Browse SmartApp Templates 
Location
