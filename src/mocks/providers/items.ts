import { Injectable } from '@angular/core';

import { Item } from '../../models/item';

@Injectable()
export class Items {
  items: Item[] = [];

  defaultItem: any = {
    "name": "Fire alarm",
    "profilePic": "assets/imgs/icons/fire-icon.png",
    "about": "12/12/17 19:20:33",
  };


  constructor() {
    let items = [
      {
        "name": "Fire alarm",
        "profilePic": "assets/imgs/icons/fire-icon.png",
        "about": "12/12/18 19:20:33",
      },
      {
        "name": "Water leakage",
        "profilePic": "assets/imgs/icons/water-icon.png",
        "about": "10/12/18 16:10:23",
      },
      {
        "name": "Power surge",
        "profilePic": "assets/imgs/icons/power-icon.png",
        "about": "08/11/18 12:20:33",
      },
      {
        "name": "Fire warning",
        "profilePic": "assets/imgs/icons/fire-icon.png",
        "about": "10/10/18 16:10:23",
      },
      {
        "name": "Water leakage",
        "profilePic": "assets/imgs/icons/water-icon.png",
        "about": "12/09/17 11:20:33",
      },
      {
        "name": "Fire alarm",
        "profilePic": "assets/imgs/icons/fire-icon.png",
        "about": "23/08/17 09:20:33",
      },
      {
        "name": "Smoke alarm",
        "profilePic": "assets/imgs/icons/smoke-icon.png",
        "about": "11/07/17 08:20:33",
      },
      {
        "name": "Water leakage",
        "profilePic": "assets/imgs/icons/water-icon.png",
        "about": "12/09/17 11:20:33",
      },
      {
        "name": "Fire alarm",
        "profilePic": "assets/imgs/icons/fire-icon.png",
        "about": "23/08/17 09:20:33",
      },
      {
        "name": "Smoke alarm",
        "profilePic": "assets/imgs/icons/smoke-icon.png",
        "about": "11/07/17 08:20:33",
      }
    ];

    for (let item of items) {
      this.items.push(new Item(item));
    }
  }

  query(params?: any) {
    if (!params) {
      return this.items;
    }

    return this.items.filter((item) => {
      for (let key in params) {
        let field = item[key];
        if (typeof field == 'string' && field.toLowerCase().indexOf(params[key].toLowerCase()) >= 0) {
          return item;
        } else if (field == params[key]) {
          return item;
        }
      }
      return null;
    });
  }

  add(item: Item) {
    this.items.push(item);
  }

  delete(item: Item) {
    this.items.splice(this.items.indexOf(item), 1);
  }
}
