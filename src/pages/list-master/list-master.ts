import { Component } from '@angular/core';
import { IonicPage, ModalController, NavController } from 'ionic-angular';

import { Item } from '../../models/item';
import { Items } from '../../providers';
import { HttpClient } from '../../../node_modules/@angular/common/http';



@IonicPage()
@Component({
  selector: 'page-list-master',
  templateUrl: 'list-master.html'
})
export class ListMasterPage {
  currentItems: Item[];
  data: any;
  showAlert: boolean;
  alertMsg: string;

  constructor(public navCtrl: NavController, public items: Items, public modalCtrl: ModalController, private http: HttpClient) {
    this.currentItems = this.items.query();
    setTimeout(() => {
      setInterval(()=> this.getConfig(),2000);
    }, 2000);
  }

  
  



  /**
   * The view loaded, let's query our items for the list
   */
  ionViewDidLoad() {
  }

  /**
   * Prompt the user to add a new item. This shows our ItemCreatePage in a
   * modal and then adds the new item to our data source if the user created one.
   */
  addItem() {
    let addModal = this.modalCtrl.create('ItemCreatePage');
    addModal.onDidDismiss(item => {
      if (item) {
        this.items.add(item);
      }
    })
    addModal.present();
  }

  /**
   * Delete an item from the list of items.
   */
  deleteItem(item) {
    this.items.delete(item);
  }

  /**
   * Navigate to the detail page for this item.
   */
  openItem(item: Item) {
    this.navCtrl.push('ItemDetailPage', {
      item: item
    });
  }
  getConfig() {
    return this.http.get('https://smartinsure-ef9ba.firebaseio.com/rest/saving-data/fireblog/smartinsure-ef9ba.json')
      .subscribe((data: any) => {
      this.data = data;
      if (data.data === 'A'){
        this.showAlert = true;
        this.alertMsg = 'power usage outage!!!!!!!!!';
       
      }
      else if(data.data === 'B'){
        this.showAlert = true;
        this.alertMsg = 'Movment detacted! Your house might be break!!!!!!!!!'
      }
      else if(data.data === 'C'){
        this.showAlert = true;
        this.alertMsg = 'Tempreture SUPER HIGH!! Fire!!!!!!!!!'
      }
      else {
          this.showAlert = false;
        
      }
      }
      );
  }
}
