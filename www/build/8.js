webpackJsonp([8],{

/***/ 332:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListMasterPageModule", function() { return ListMasterPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngx_translate_core__ = __webpack_require__(117);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(116);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__list_master__ = __webpack_require__(345);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__node_modules_angular_common_http__ = __webpack_require__(131);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var ListMasterPageModule = /** @class */ (function () {
    function ListMasterPageModule() {
    }
    ListMasterPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__list_master__["a" /* ListMasterPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_4__node_modules_angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_3__list_master__["a" /* ListMasterPage */]),
                __WEBPACK_IMPORTED_MODULE_1__ngx_translate_core__["b" /* TranslateModule */].forChild()
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_3__list_master__["a" /* ListMasterPage */]
            ]
        })
    ], ListMasterPageModule);
    return ListMasterPageModule;
}());

//# sourceMappingURL=list-master.module.js.map

/***/ }),

/***/ 345:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListMasterPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(116);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers__ = __webpack_require__(118);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__node_modules_angular_common_http__ = __webpack_require__(131);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ListMasterPage = /** @class */ (function () {
    function ListMasterPage(navCtrl, items, modalCtrl, http) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.items = items;
        this.modalCtrl = modalCtrl;
        this.http = http;
        this.currentItems = this.items.query();
        setTimeout(function () {
            setInterval(function () { return _this.getConfig(); }, 2000);
        }, 2000);
    }
    /**
     * The view loaded, let's query our items for the list
     */
    ListMasterPage.prototype.ionViewDidLoad = function () {
    };
    /**
     * Prompt the user to add a new item. This shows our ItemCreatePage in a
     * modal and then adds the new item to our data source if the user created one.
     */
    ListMasterPage.prototype.addItem = function () {
        var _this = this;
        var addModal = this.modalCtrl.create('ItemCreatePage');
        addModal.onDidDismiss(function (item) {
            if (item) {
                _this.items.add(item);
            }
        });
        addModal.present();
    };
    /**
     * Delete an item from the list of items.
     */
    ListMasterPage.prototype.deleteItem = function (item) {
        this.items.delete(item);
    };
    /**
     * Navigate to the detail page for this item.
     */
    ListMasterPage.prototype.openItem = function (item) {
        this.navCtrl.push('ItemDetailPage', {
            item: item
        });
    };
    ListMasterPage.prototype.getConfig = function () {
        var _this = this;
        return this.http.get('https://smartinsure-ef9ba.firebaseio.com/rest/saving-data/fireblog/smartinsure-ef9ba.json')
            .subscribe(function (data) {
            _this.data = data;
            if (data.data === 'A') {
                _this.showAlert = true;
                _this.alertMsg = 'power usage outage!!!!!!!!!';
            }
            else if (data.data === 'B') {
                _this.showAlert = true;
                _this.alertMsg = 'Movment detacted! Your house might be break!!!!!!!!!';
            }
            else if (data.data === 'C') {
                _this.showAlert = true;
                _this.alertMsg = 'Tempreture SUPER HIGH!! Fire!!!!!!!!!';
            }
            else {
                _this.showAlert = false;
            }
        });
    };
    ListMasterPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-list-master',template:/*ion-inline-start:"/Users/opr1004/Desktop/hack2018/demo/hackathon2018/src/pages/list-master/list-master.html"*/'<ion-header>\n\n  <ion-navbar>\n    <ion-title>{{ \'LIST_MASTER_SUB_TITLE\' | translate }}</ion-title>\n\n    <ion-buttons end>\n      <button ion-button icon-only (click)="addItem()">\n        <ion-icon name="add"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n\n</ion-header>\n<!-- \n<ion-content>\n\n  <ion-list>\n    <ion-item-sliding *ngFor="let item of currentItems">\n      <button ion-item (click)="openItem(item)">\n        <ion-avatar item-start>\n          <img [src]="item.profilePic" />\n        </ion-avatar>\n        <h2>{{item.name}}</h2>\n        <p>{{item.about}}</p>\n        <ion-note item-end *ngIf="item.note">{{item.note}}</ion-note>\n      </button>\n\n      <ion-item-options>\n        <button ion-button color="danger" (click)="deleteItem(item)">\n          {{ \'DELETE_BUTTON\' | translate }}\n        </button>\n      </ion-item-options>\n    </ion-item-sliding>\n  </ion-list>\n</ion-content> -->\n\n<ion-content class="card-background-page" >\n  <ion-card class="bg4" *ngIf="showAlert">\n    <ion-card-header style="padding-top: 0px;">\n      <span class="text-white"> Instant Alert(s)</span>\n    </ion-card-header>\n    <div class="splash-intro">\n      <img src="assets/imgs/fire-alert.png" />\n    </div>\n    <ion-card-content class="text-white">{{this.alertMsg}}</ion-card-content>\n    <button ion-button round small>Snooze</button>\n    <button ion-button color="light" round small>Dismiss</button>\n  </ion-card>\n\n      <!-- <button ion-button >Default</button> -->\n  \n\n  <!-- <h6>List of added devices</h6> -->\n  <!-- <ion-card *ngFor="let device of devices">\n      <img src="assets/img/nin-live.png"/>\n      <div class="card-title">{{device.deviceName}}</div>\n      <div class="card-subtitle">{{device.notification}}</div>\n    </ion-card> -->\n  <ion-card>\n    <img src="assets/imgs/thumbnails/power-monitor-thumbnail.png" />\n    <div class="card-title">Power monitor</div>\n    <div class="card-subtitle">72 Notifications</div>\n  </ion-card>\n\n  <ion-card>\n    <img src="assets/imgs/thumbnails/fire-alarm-thumbnail.png" />\n    <div class="card-title">Fire alarm</div>\n    <div class="card-subtitle">64 Notifications</div>\n  </ion-card>\n\n  <ion-card>\n    <img src="assets/imgs/thumbnails/water-leakage-thumbnail.png" />\n    <div class="card-title"> Water leakage sensor</div>\n    <div class="card-subtitle">64 Notifications</div>\n  </ion-card>\n\n  <ion-card>\n    <img src="assets/imgs/thumbnails/smoke-detector-thumbnail.png" />\n    <div class="card-title">Smoke detectors</div>\n    <div class="card-subtitle">28 Notifications</div>\n  </ion-card>\n\n</ion-content>'/*ion-inline-end:"/Users/opr1004/Desktop/hack2018/demo/hackathon2018/src/pages/list-master/list-master.html"*/
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__providers__["b" /* Items */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__providers__["b" /* Items */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* ModalController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* ModalController */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_3__node_modules_angular_common_http__["a" /* HttpClient */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__node_modules_angular_common_http__["a" /* HttpClient */]) === "function" && _d || Object])
    ], ListMasterPage);
    return ListMasterPage;
    var _a, _b, _c, _d;
}());

//# sourceMappingURL=list-master.js.map

/***/ })

});
//# sourceMappingURL=8.js.map